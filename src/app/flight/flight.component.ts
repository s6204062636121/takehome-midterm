import { Component, OnInit } from '@angular/core';
import { Flight } from '../flight';
import { PageService } from '../service/page.service';
import { FormBuilder , FormGroup } from '@angular/forms';
import { Validators } from '@angular/forms';

@Component({
  selector: 'app-flight',
  templateUrl: './flight.component.html',
  styleUrls: ['./flight.component.css']
})
export class FlightComponent implements OnInit {
  flights !: Flight[];
  flightForm !: FormGroup ;
  date = false ;
  constructor(private fb: FormBuilder , private pageService:PageService){

  }

  ngOnInit(): void {
    this.flightForm = this.fb.group({
      FlightName:['', [Validators.required, Validators.min(1)]],
      FlightFrom:['', [Validators.required]],
      FlightTo:['', [Validators.required]],
      FlightType:['', [Validators.required]],
      FlightDepart:['', [Validators.required]],
      FlightArrival:['',[Validators.required]],
      FlightAdult:['', [Validators.required, Validators.min(1)]],
      FlightChildren:['', [Validators.required,  Validators.min(0)]],
      FlightInfants:['',[Validators.required,  Validators.min(0)]],
    }, {validator : this.compareDate('FlightDepart', 'FlightArrival')});
    this.getFlightPage();
  }
  getFlightPage(){
    this.flights = this.pageService.getFlight();
  }
  onSubmitForm(f:FormGroup) :void{
    let form_record = new Flight(f.get('FlightName')?.value,
                                  f.get('FlightFrom')?.value,
                                  f.get('FlightTo')?.value,
                                  f.get('FlightType')?.value,
                                  f.get('FlightDepart')?.value,
                                  f.get('FlightArrival')?.value,
                                  f.get('FlightAdult')?.value,
                                  f.get('FlightChildren')?.value,
                                  f.get('FlightInfants')?.value,);
    this.pageService.addFlight(form_record);
  }
  compareDate(from: string, to: string){
    return(group: FormGroup): {[key: string]: any}=>{
      let f = group.controls[from] ;
      let t = group.controls[to] ;
      if(f.value > t.value){
        return {
          dates: "Date Arrival More than Date Depart OR Today"
        };
      }
      return {};
    }
   }
}

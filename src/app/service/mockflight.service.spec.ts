import { TestBed } from '@angular/core/testing';

import { MockflightService } from './mockflight.service';

describe('MockflightService', () => {
  let service: MockflightService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(MockflightService);
  });

  it('should created an instacve', () => {
    expect(new MockflightService()).toBeTruthy();
  });
});

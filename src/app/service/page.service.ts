import { Injectable } from '@angular/core';
import { Flight } from '../flight';
import { MockflightService } from './mockflight.service';

@Injectable({
  providedIn: 'root'
})
export class PageService {
  flight : Flight[] = []

  constructor() {
    this.flight = MockflightService.mflight ;
  }
  getFlight(): Flight[]{
    return this.flight;
  }

  addFlight(f:Flight): void{
    this.flight.push(f);
  }
}
